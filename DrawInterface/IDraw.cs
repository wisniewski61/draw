﻿using System.Drawing;

namespace DrawInterface
{
    public interface IDraw
    {
        string GetName();
        Bitmap Transfrom(Bitmap toTransform);
    }
}
