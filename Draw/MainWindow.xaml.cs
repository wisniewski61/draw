﻿using Draw.Helpers;
using DrawInterface;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Draw
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string pluginsFolderLocation = "plugins";
        private Point currentPoint = new Point();
        private Color? currentColor = Colors.White;
        private UndoRedoHelper undoRedoHelper;
        private List<IDraw> plugins;
        private ObservableCollection<string> pluginsListCombo;

        public MainWindow()
        {
            InitializeComponent();
            undoRedoHelper = new UndoRedoHelper();
            plugins = loadPlugins();
            pluginsListCombo = new ObservableCollection<string>();

            foreach (IDraw plugin in plugins)
                pluginsListCombo.Add(plugin.GetName());
            pluginsCombobox.ItemsSource = pluginsListCombo;
            pluginsCombobox.SelectedIndex = 0;
        }

        private List<IDraw> loadPlugins()
        {
            List<IDraw> pluginsList = new List<IDraw>();
            foreach (string dll in Directory.GetFiles(pluginsFolderLocation, "*.dll"))
            {
                IDraw loaded = loadPluginFromAssembly(dll);
                if (loaded != null)
                    pluginsList.Add(loaded);
            }//foreach
            return pluginsList;
        }//loadPlugin()

        private IDraw loadPluginFromAssembly(string dll)
        {
            var a = Assembly.LoadFrom(dll);
            var types = a.GetTypes();
            foreach (var t in types)
            {
                if (t.IsClass && t.IsPublic && typeof(IDraw).IsAssignableFrom(t))
                {
                    var o = (IDraw)Activator.CreateInstance(t);
                    string name = o.GetName();
                    Console.WriteLine(name);
                    return o;
                }//if
            }//for

            return null;
        }//loadPluginFromAssembly()

        // source: https://stackoverflow.com/questions/17089382/wpf-color-picker-implementation/17090082#17090082
        private void ClrPcker_Background_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            String color = e.NewValue.ToString();
            currentColor = e.NewValue;
            //color = "#" + ClrPcker_Background.SelectedColor.R.ToString() + ClrPcker_Background.SelectedColor.G.ToString() + ClrPcker_Background.SelectedColor.B.ToString();
            Console.WriteLine(color);
        }

        // source: https://stackoverflow.com/questions/16037753/wpf-drawing-on-canvas-with-mouse-events/16038005#16038005
        private void Canvas_MouseDown_1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            undoRedoHelper.AddOperation(GetRenderedTargetBitmap());
            if (e.ButtonState == MouseButtonState.Pressed)
                currentPoint = e.GetPosition(paintSurface);
        }

        private void Canvas_MouseMove_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Line line = new Line();

                SolidColorBrush brush = new SolidColorBrush();
                brush.Color = currentColor ?? Colors.Black;
                line.Stroke = brush;
                line.X1 = currentPoint.X;
                line.Y1 = currentPoint.Y;
                line.X2 = e.GetPosition(paintSurface).X;
                line.Y2 = e.GetPosition(paintSurface).Y;

                currentPoint = e.GetPosition(paintSurface);

                paintSurface.Children.Add(line);
            }
        }

        private void FileOpenMenu_Clicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                ImageBrush brush = new ImageBrush();
                brush.ImageSource = new BitmapImage(new Uri(openFileDialog.FileName));
                paintSurface.Background = brush;
            }
        }

        private void FileNew_Clicked(object sender, RoutedEventArgs e)
        {
            paintSurface.Children.Clear();
            paintSurface.Background = new ImageBrush();
        }

        private RenderTargetBitmap GetRenderedTargetBitmap()
        {
            Rect rect = new Rect(paintSurface.RenderSize);
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)rect.Right,
              (int)rect.Bottom, 96d, 96d, System.Windows.Media.PixelFormats.Default);
            rtb.Render(paintSurface);
            return rtb;
        }

        private void FileSave_Clicked(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "PNG file (*.png)|*.png|JPG file (*.jpg)|*.jpg";
            if (saveFileDialog.ShowDialog() == true)
            {
                // source: https://jasonkemp.ca/blog/how-to-save-xaml-as-an-image/
                RenderTargetBitmap rtb = GetRenderedTargetBitmap();
                //endcode as PNG
                BitmapEncoder pngEncoder = new PngBitmapEncoder();
                pngEncoder.Frames.Add(BitmapFrame.Create(rtb));

                //save to memory stream
                System.IO.MemoryStream ms = new System.IO.MemoryStream();

                pngEncoder.Save(ms);
                ms.Close();
                System.IO.File.WriteAllBytes(saveFileDialog.FileName, ms.ToArray());
            }//if
        }//FileSave_Clicked()

        private void Undo_Clicked(object sender, RoutedEventArgs e)
        {            
            if (undoRedoHelper.UndoSize() > 0)
            {
                paintSurface.Children.Clear();
                ImageBrush brush = new ImageBrush();
                brush.ImageSource = undoRedoHelper.Undo();
                paintSurface.Background = brush;
            }//if
        }//Undo_Clicked()

        private void Redo_Clicked(object sender, RoutedEventArgs e)
        {
            // TODO: implement redo
            if (undoRedoHelper.RedoSize() > 0)
            {
                paintSurface.Children.Clear();
                ImageBrush brush = new ImageBrush();
                brush.ImageSource = undoRedoHelper.Redo();
                paintSurface.Background = brush;
            }//if
        }//Redo_Clicked()

        private void Execute_Clicked(object sender, RoutedEventArgs e)
        {
            if (plugins.Count == 0)
                return;
            undoRedoHelper.AddOperation(GetRenderedTargetBitmap());
            string pluginName = pluginsCombobox.SelectedValue.ToString();
            IDraw selectedPlugin = plugins.AsQueryable().Where(p => p.GetName().Equals(pluginName)).FirstOrDefault();
            System.Drawing.Bitmap bitmap = GetBitmap();

            bitmap = selectedPlugin.Transfrom(bitmap);

            paintSurface.Children.Clear();
            ImageBrush brush = new ImageBrush();
            brush.ImageSource = BitmapToImageSource(bitmap);
            paintSurface.Background = brush;
        }//Execute_Clicked()

        // source: https://stackoverflow.com/questions/20083210/convert-rendertargetbitmap-to-bitmap/20083597#20083597
        private System.Drawing.Bitmap GetBitmap()
        {
            RenderTargetBitmap bmpRen = GetRenderedTargetBitmap();

            MemoryStream stream = new MemoryStream();
            BitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bmpRen));
            encoder.Save(stream);

            return new System.Drawing.Bitmap(stream);
        }//GetBitmap()

        // source: https://stackoverflow.com/questions/34361260/how-to-convert-icon-bitmap-to-imagesource/34361396#34361396
        private BitmapImage BitmapToImageSource(System.Drawing.Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }//using
        }//BitmapImage
    }
}
