﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Draw.Helpers
{
    class UndoRedoHelper
    {
        private Stack<RenderTargetBitmap> undoStack;
        private Stack<RenderTargetBitmap> redoStack;

        public UndoRedoHelper() {
            undoStack = new Stack<RenderTargetBitmap>();
            redoStack = new Stack<RenderTargetBitmap>();
        }//UndoRedoHelper()

        public void AddOperation(RenderTargetBitmap operation)
        {
            undoStack.Push(operation);
        }//AddOperation()

        public RenderTargetBitmap Undo()
        {
            RenderTargetBitmap toRet = undoStack.Pop();
            redoStack.Push(toRet);
            return toRet;
        }//Undo()

        public RenderTargetBitmap Redo()
        {
            RenderTargetBitmap toRet = redoStack.Pop();
            undoStack.Push(toRet);
            return toRet;
        }//Redo

        public int UndoSize()
        {
            return undoStack.Count;
        }

        public int RedoSize()
        {
            return redoStack.Count;
        }
    }
}
