﻿using DrawInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace RotatePlugin
{
    public class Rotate : IDraw
    {
        private const string name = "Rotate";

        public string GetName()
        {
            return name;
        }//GetName()

        public Bitmap Transfrom(Bitmap toTransform)
        {
            toTransform.RotateFlip(RotateFlipType.Rotate180FlipNone);
            return toTransform;
        }
    }
}
